Установка приложения MySQL + WordPress + FPM + nginx.
1. Для простоты настройки выключить selinux - setnfore 0
2. Для простоты настройки создать самоподписанный сертификат.
 - openssl genrsa > /etc/letsencrypt/live/gbwordpress.local/privkey.pem
 - openssl req -new -x509 -key /etc/letsencrypt/live/gbwordpress.local/privkey.pem > /etc/letsencrypt/live/gbwordpress.local/fullchain.pem
3. Создать ansible.cfg, указать в нем каталог для скачивания внешних ролей
4. установить их (mysql, php, nginx)
5. написать роль для установки WordPress
